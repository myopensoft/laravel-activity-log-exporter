# Laravel Activity Log Exporter

[![Latest Version on Packagist](https://img.shields.io/packagist/v/myopensoft/activity-log-exporter.svg?style=flat-square)](https://packagist.org/packages/myopensoft/activity-log-exporter)
[![GitHub Tests Action Status](https://img.shields.io/github/actions/workflow/status/myopensoft/activity-log-exporter/run-tests.yml?branch=main&label=tests&style=flat-square)](https://github.com/myopensoft/activity-log-exporter/actions?query=workflow%3Arun-tests+branch%3Amain)
[![GitHub Code Style Action Status](https://img.shields.io/github/actions/workflow/status/myopensoft/activity-log-exporter/fix-php-code-style-issues.yml?branch=main&label=code%20style&style=flat-square)](https://github.com/myopensoft/activity-log-exporter/actions?query=workflow%3A"Fix+PHP+code+style+issues"+branch%3Amain)
[![Total Downloads](https://img.shields.io/packagist/dt/myopensoft/activity-log-exporter.svg?style=flat-square)](https://packagist.org/packages/myopensoft/activity-log-exporter)

To export old activity log as csv file and delete it after that from database.

## Installation

You can install the package via composer:

```bash
composer require myopensoft/activity-log-exporter
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --tag="activity-log-exporter-migrations"
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --tag="activity-log-exporter-config"
```

This is the contents of the published config file:

```php
return [
];
```

Optionally, you can publish the views using

```bash
php artisan vendor:publish --tag="activity-log-exporter-views"
```

## Usage

```php
$activityLogExporter = new MyOpensoft\ActivityLogExporter();
echo $activityLogExporter->echoPhrase('Hello, MyOpensoft!');
```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Muhammad Syafiq Bin Zainuddin](https://github.com/lomotech)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
