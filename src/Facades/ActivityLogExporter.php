<?php

namespace MyOpensoft\ActivityLogExporter\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \MyOpensoft\ActivityLogExporter\ActivityLogExporter
 */
class ActivityLogExporter extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \MyOpensoft\ActivityLogExporter\ActivityLogExporter::class;
    }
}
