<?php

namespace MyOpensoft\ActivityLogExporter\Commands;

use Illuminate\Console\Command;

class ActivityLogExporterCommand extends Command
{
    public $signature = 'activity-log-exporter';

    public $description = 'My command';

    public function handle(): int
    {
        $this->comment('All done');

        return self::SUCCESS;
    }
}
