<?php

namespace MyOpensoft\ActivityLogExporter;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use MyOpensoft\ActivityLogExporter\Commands\ActivityLogExporterCommand;

class ActivityLogExporterServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('activity-log-exporter')
            ->hasConfigFile()
            ->hasCommand(ActivityLogExporterCommand::class);
    }
}
